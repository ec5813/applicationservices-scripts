#!/bin/bash -
#finds a file in your home directory
declare file_name=$*
declare exit_code
declare files

files=$( (find ~ -name $file_name) )

checkIfExists() {
if [[ $files != "" ]]; then
	echo "The filename: $file_name exists"
	exit_code=0
else
	echo "The filename: $file_name cannot be found"
	exit_code=1
fi
}

checkIfExists

exit $exit_code
