#!/bin/bash
range1=$1
range2=$2
isNumber='^[0-9]+$'

#Task 1 use sed to emulate grep searching for lines starting with option
# sed -n -r '/.(option).*/p' dhcpd.conf 

#Tast 2 delete all blank lines in the file
# sed -i -e '/^\s*$/d' dhcpd.conf 

#Task 3 Add the following line to the beginning of the file # NAsp19 sed DHCP Configuration
# sed -i '1s/^/# NAsp19 sed DHCP Configuration\n/' dhcpd.conf

#Task 4 Remove all comments at the end of the line
# sed -i 's/^[^#]\(.*\)#.*$/ \1/' dhcpd.conf

#Task 5 Change the router IP Address to 10.10.0.1
# sed -i -r 's/(option.[[:space:]]routers.[[:space:]]+).+\.([0-9]+.[0-9]+.[0-9]+.[0-9]+)/\110.10..1/' dhcpd.conf

#Task 6 Report the range of IP addresses. 
# sed -r -n 's/(range)[[:space:]]+.[0-9]+.[0-9]+.[0-9]+\.([0-9]+)[[:space:]]+.[0-9]+.[0-9]+.[0-9]+\.([0-9]+)./IP range is:\2-\3/p' dhcpd.conf

#Task 7 Prompt the user to change the address range (last octet in the /24 network). 
if [ $range2 == "" ]; then
  echo "Please enter an upper bound"
  exit 1
elif ! ([[ $range1 =~ $isNumber ]] && [[ $range2 =~ $isNumber ]]); then
  echo "Please enter a positive integer"
  exit 1
elif [[ $range2 -gt $range1 ]]; then
  echo "Please enter a proper range"
  exit 1
fi

sed -e -i "/.*range/ s/\.[0-9]*[ ;]/\.$range1 /g1" -e "/.*range/ s/\.[0-9]*[ ;]/\.$range2 /g2" dhcpd.conf
