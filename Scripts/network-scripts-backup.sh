#!/bin/bash
declare scriptLocation="/etc/sysconfig/network-scripts/"
declare saveLocation=$1

files=$( ls $scriptLocation )

if [[ $saveLocation == "" ]]; then
	$( mkdir -p /var/tmp/backup )
	saveLocation="/var/tmp/backup"
fi

echo $saveLocation

for file in $files; do
	backup=".bak"
	backup="$file$backup"
	$( cp $scriptLocation$file $saveLocation/$backup )
done
