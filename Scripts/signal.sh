#!/bin/bash -

declare pid=$1
declare sigId=$2
declare validIds=("SIGHUP" "SIGINT" "SIGTERM" "SIGUSR1")

printValidIds() {
	echo "Valid signals are ${validIds[*]}"
}

if [[ $sigId == "" ]]; then
	printf "Not enough arguments supplied.\nPlease enter it in the form of pid signalId\n"
	printValidIds
	exit
else
	case "$sigId" in
		${validIds[0]})
			kill -${validIds[0]} $pid
			;;
		${validIds[1]})
			kill -${validIds[1]} $pid
			;;
		${validIds[2]})
			kill -${validIds[2]} $pid
			;;
		${validIds[3]})
			kill -${validIds[3]} $pid
			;;
		*)
			echo "Invalid signal ID"
			printValidIds
			;;
	esac
fi
