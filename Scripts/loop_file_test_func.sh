#!/bin/bash -
#finds a file in your home directory
declare file_names=$@

checkIfExists() {
if [[ $fileLocation != "" ]]; then
	echo "The filename: $file exists"
else
	echo "The filename: $file cannot be found"
fi
}

loopFiles() {
for file in $file_names; do
	fileLocation=$( (find ~ -name $file) )
	checkIfExists $fileLocation $file
done
}
