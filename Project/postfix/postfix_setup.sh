#!/bin/bash 

mailname=$1
port=$2
key=$3
cert=$4
username=$5
domain=$6

yum install -y postfix

cp ./postfix/master.cf /etc/postfix/master.cf 
cp ./postfix/main.cf /etc/postfix/main.cf

##TODO:
#use sed to change inet_interfaces to = all
#use sed to change mydestination = $myhostname, localhost.$mydomain, localhost, $mydomain
#use sed to make home_mailbox = Maildir/

sed -r -i -e 's/^(inet_interfaces.*)/#\1/' -e 's/^#(inet_interfaces = all)/\1/' -e 's/^(mydestination.*)/\1, $mydomain/' -e 's/^#(home_mailbox = Maildir.*)/\1/' /etc/postfix/main.cf
sed -r -i -e '$ a\smtpd_sasl_type = dovecot' -e '$ a\smtpd_sasl_path = inet:'"$mailname"':'"$port" -e '$ a\smtpd_sasl_auth_enable = yes' -e '$ a\smtpd_relay_restrictions = permit_mynetworks, permit_sasl_authenticated, reject_unauth_destination' /etc/postfix/main.cf
sed -r -i 's/^#(submission inet.*)/\1/' /etc/postfix/master.cf 
sed -r -i 's/^(#|)([[:space:]]*-o[[:space:]]*smtpd_tls_security_level=).*/\2encrypt/' /etc/postfix/master.cf 
sed -r -i '0,/smtpd_sasl_auth_enable/s/^(#|)([[:space:]]*-o[[:space:]]*smtpd_sasl_auth_enable=).*/\2yes/' /etc/postfix/master.cf
sed -r -i -e 's/^(#|)([[:space:]]*-o[[:space:]]*smtpd_sasl_type=).*/\2dovecot/' -e '/submission inet/a\ -o smtpd_sasl_type=dovecot' /etc/postfix/master.cf
sed -r -i -e 's/^(#|)([[:space:]]*-o[[:space:]]*smtpd_sasl_path=).*/\2private\/auth/' -e '/submission inet/a\ -o smtpd_sasl_path=private\/auth' /etc/postfix/master.cf
sed -r -i -e 's/^(#|)([[:space:]]*-o[[:space:]]*smtpd_sasl_security_options=).*/\2noanonymous/' -e '/submission inet/a\ -o smtpd_sasl_security_options=noanonymous' /etc/postfix/master.cf
sed -r -i -e 's/^(#|)([[:space:]]*-o[[:space:]]*smtpd_sasl_local_domain=).*/\2$myhostname/' -e '/submission inet/a\ -o smtpd_sasl_local_domain=$myhostname' /etc/postfix/master.cf
sed -r -i '0,/smtpd_client_restrictions/s/^(#|)([[:space:]]*-o[[:space:]]*smtpd_client_restrictions=).*/\2permit_sasl_authenticated,reject/' /etc/postfix/master.cf
sed -r -i -e 's/^(#|)([[:space:]]*-o[[:space:]]*smtpd_sender_login_maps=).*/\2hash:\/etc\/postfix\/virtua/' -e '/submission inet/a\ -o smtpd_sender_login_maps=hash:\/etc\/postfix\/virtual' /etc/postfix/master.cf


openssl req -x509 -nodes -days 730 -newkey rsa:8192 -keyout $key -out $cert -subj '/commonName='"$domain"'/emailAddress='"$username"'@'"$domain"'/'

sed -r -i -e '$ a\smtpd_tls_security_level = may' -e '$ a\smtpd_tls_key_file = '"$key" -e '$ a\smtpd_tls_cert_file = '"$cert" -e '$ a\smtpd_tls_loglevel = 1' -e '$ a\smtpd_tls_session_cache_timeout = 3600s' -e '$ a\smtpd_tls_session_cache_database = btree:\/var\/lib\/postfix\/smtpd_tls_cache' -e '$ a\tls_random_source = dev:\/dev\/urandom' -e '$ a\tls_random_exchange_name = \/var\/lib\/postfix\/prng_exch' -e '$ a\smtpd_tls_auth_only = yes' /etc/postfix/main.cf

systemctl enable postfix
systemctl start postfix