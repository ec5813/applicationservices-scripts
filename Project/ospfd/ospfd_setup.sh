#!/bin/bash - 

. ./split.sh

yum install -y quagga

netInts=($(splitComma $1))
wifi=$2
wifiInts=($(splitComma $3))
netIPs=($(splitComma $4))
wifiIPs=($(splitComma $5))
numInts=${#netInts[@]}
totalInts=( "${netInts[@]}" )
totalIPs=( "${netIPs[@]}" )

if [[ $wifi == "true" ]]; then
	numInts=$(( $numInts + ${#wifiInts[@]} ))
	totalInts=( "${totalInts[@]}" "${wifiInts[@]}" )
	totalIPs=( "${totalIPs[@]}" "${wifiIPs[@]}" )
fi

cp ./ospfd/ospfd.conf /etc/quagga/
#todo: for every interface create a
#interface $interfaceName
#!
for ((i=0; i<$numInts; i++)); do
	int=${totalInts[$i]}
	sed -i -r -e '$ a\interface '$int -e '$ a\!' /etc/quagga/ospfd.conf
done

#todo: for every IP create a network in router ospf
#router ospf
#  ospf router-id $mainIntIP
#  network #interfacesIp
#!
sed -i -r -e '$ a\router ospf' /etc/quagga/ospfd.conf
outIPSplit=($(splitSlash ${totalIPs[0]}))
outIP=${outIPSplit[0]}
sed -i -r -e '$ a\ ospf router-id '$outIP /etc/quagga/ospfd.conf
for ((i=0; i<$numInts; i++)); do
	sed -i -r -e '$ a\ network '${totalIPs[$i]}' area 0.0.0.0' /etc/quagga/ospfd.conf
done
sed -i -r -e '$ a\!' /etc/quagga/ospfd.conf

cp ./ospfd/zebra.conf /etc/quagga/

#todo: for every interface create an interface
#interface $interfaceName
#  description name
#  ip address $interfacesIPwithNetMask
#  ipv6 nd suppress-ra
#!
for ((i=0; i<$numInts; i++)); do
	sed -i -r -e '$ a\interface '${totalInts[$i]} -e '$ a\ description name' -e '$ a\ ip address '${totalIPs[$i]} -e '$ a\ ipv6 nd suppress-ra' -e '$ a\!' /etc/quagga/zebra.conf
done

chown -R quagga:quagga /etc/quagga

cp ./ospfd/sysctl.conf /etc/sysctl.conf

sed -i -r -e '$ a\net.ipv4.ip_forward=1' /etc/sysctl.conf

/sbin/sysctl -w net.ipv4.ip_forward=1

systemctl start ospfd zebra
systemctl enable ospfd zebra