#!/bin/bash - 
 declare -A options
 
 set -f
 
 splitIntoHash() {
   tmpOptions=(${1//$2/ })
   value=(${tmpOptions[@]:1})
   options[${tmpOptions[0]}]=${value[@]}
 }

 usageMsg="Please use one of the following: -r to setup the router, -m to setup the mail"

 if [[ $1 == "" ]]; then
 	echo $usageMsg
 	exit 1
 fi
 
 while IFS='' read -r line || [[ -n "$line" ]]; do
 	if [[ $line != "#"* ]]; then
		splitIntoHash "$line" "="
	fi
 #  echo ${!options[@]}
 #  echo ${options[@]}
 done < "main.conf"

 hostname=""

 if [[ $1 == "-r" ]]; then
 	hostname=${options["hostname"]}
 elif [[ $1 == "-m" ]]; then
 	hostname=${options["mailHostname"]}
 fi

. ./base_setup/base_configuration.sh $hostname

. ./base_setup/selinux_setup.sh

if [[ $1 == "-r" ]]; then


	if [[ ${options["network"]} == "true" ]]; then
		echo "Setting up network"
		if [[ ${options["wifi"]} == "true" ]]; then
			echo "Setting up wifi"
		else
			echo "Wifi has been skipped"
		fi
		networkArray=(${options["wifi"]}
			${options["networkInts"]}
			${options["networkIP"]}
			${options["networkID"]}
			${options["networkDNS"]}
			${options["wifiInts"]}
			${options["wifiIP"]}
			${options["wifiID"]}
			${options["wifiChan"]}
			${options["wifiName"]}
			${options["wifiEncrypt"]}
			${options["wifiPass"]}
			${options["networkGW"]}
			${options["networkBootProto"]})
		. ./network/network_setup.sh "${networkArray[@]}"
	else
		echo "Network has been skipped"
	fi

	if [[ ${options["iptables"]} == "true" ]]; then
		echo "Setting up iptables"
		iptablesArray=(${options["networkInts"]}
			${options["networkIP"]}
			${options["networkID"]}
			${options["wifi"]}
		${options["wifiInts"]})
		. ./iptables/iptables_setup.sh "${iptablesArray[@]}"
	else
		echo "Iptables has been skipped"
	fi

	if [[ ${options["ospf"]} == "true" ]]; then
		ospfdArray=(${options["networkInts"]}
			${options["wifi"]}
			${options["wifiInts"]}
			${options["networkIP"]}
			${options["wifiIP"]})
		echo "Setting up ospf"
		. ./ospfd/ospfd_setup.sh "${ospfdArray[@]}"
	else
		echo "Ospf has been skipped"
	fi

	if [[ ${options["dhcp"]} == "true" ]]; then
		dhcpArray=(${options["dhcpInt"]}
			${options["domain"]}
			${options["networkIP"]}
			${options["networkID"]}
			${options["wifi"]}
			${options["wifiIP"]}
			${options["wifiID"]}
			${options["dhcpStaticNet"]}
			${options["dhcpStaticIP"]}
			${options["dhcpStaticName"]}
			${options["dhcpStaticMAC"]}
			${options["dhcpIPRange"]})
		echo "Setting up dhcp"
		. ./dhcpd/dhcpd_setup.sh "${dhcpArray[@]}"
	else
		echo "Dhcp has been skipped"
	fi

	if [[ ${options["unbound"]} == "true" ]]; then
		unboundArray=(${options["unboundDNS"]}
			${options["networkID"]}
			${options["networkIP"]}
			${options["wifi"]}
			${options["wifiID"]}
			${options["wifiIP"]}
			${options["zones"]}
			${options["rZones"]})
		echo "Setting up unbound"
		. ./unbound/unbound_setup.sh "${unboundArray[@]}"
	else
		echo "Unbound has been skipped"
	fi

	if [[ ${options["nsd"]} == "true" ]]; then
		nsdArray=(${options["networkIP"]}
			${options["zones"]}
			${options["zoneFiles"]}
			${options["rZones"]}
			${options["zZoneFiles"]}
			${options["nsdARecords"]}
			${options["nsdARecordIPs"]}
			${options["hostname"]}
			${options["domain"]}
			${options["nsdMail"]})

		. ./nsd/nsd_setup.sh "${nsdArray[@]}"
	else
		echo "Nsd has been skipped"
	fi
elif [[ $1 == "-m" ]]; then
	if [[ ${options["mailNetwork"]} == "true" ]]; then
		networkArray=(${options["mailInts"]}
			" "
			" "
			${options["mailDNS"]}
			${options["mailGW"]}
			${options["mailBootProto"]})
		echo "Setting up mail network"
		. ./network/net_if_setup.sh "${networkArray[@]}"
	else
		echo "Mail network has been skipped"
	fi

	if [[ ${options["postfix"]} == "true" ]]; then
		postfixArray=(${options["mailHostname"]}
			${options["dovecotPort"]}
			${options["postfixKey"]}
			${options["postfixCert"]}
			${options["postfixUsername"]}
			${options["domain"]})
		echo "Setting up postfix"
		. ./postfix/postfix_setup.sh "${postfixArray[@]}"
	else
		echo "Postfix has been skipped"
	fi

	if [[ ${options["dovecot"]} == "true" ]]; then
		dovecotArray=(${options["dovecotLoginNets"]}
			${options["dovecotMailDir"]}
			${options["dovecotPort"]}
			${options["postfixKey"]}
			${options["postfixCert"]})
			echo "Setting up dovecot"
		. ./dovecot/dovecot_setup.sh "${dovecotArray[@]}"
	else
		echo "Dovecot has been skipped"
	fi
else
	echo $usageMsg
	exit 1
fi

echo "Finished setting up machine"
echo "Please restart the machine to finish VirtualBox Guest Additions"
exit 0