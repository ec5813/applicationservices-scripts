#!/bin/bash - 

. ./split.sh

interfaceIP=($(splitComma $1))
interfaceIP="${interfaceIP[0]}"
interfaceIP=($(splitSlash $interfaceIP))
zones=($(splitComma $2))
zoneFiles=($(splitComma $3))
rZones=($(splitComma $4))
rZoneFiles=($(splitComma $5))
aRecords=($(splitComma $6))
aRecordIPs=($(splitComma $7))
hostname=$8
domain=$9
mail=${10}
zoneNum=${#zones[@]}
rZoneNum=${#rZones[@]}
aRecordNum=${#aRecords[@]}

yum install -y nsd

cp ./nsd/nsd.conf /etc/nsd/

#sed -i -r -e 's/^([[:space:]]+ip-address:)/\1 '$(echo $interfaceIP | sed 's_/_\\/_g')'/' /etc/nsd/nsd.conf
sed -i -r -e 's/^([[:space:]]+ip-address:)/\1 '"${interfaceIP[0]}"'/' /etc/nsd/nsd.conf

for ((i=0;$i<$zoneNum;i++)); do
	zone=${zones[$i]}
	zoneFile=${zoneFiles[$i]}
	sed -i -r -e '$ a\zone:' -e '$ a\\tname: "'"$zone"'"' -e '$ a\\tzonefile: "'"$zoneFile"'"\n' /etc/nsd/nsd.conf
done

for ((i=0;$i<$rZoneNum;i++)); do
	rZone=${rZones[$i]}
	rZoneFile=${rZoneFiles[$i]}
	sed -i -r -e '$ a\zone:' -e '$ a\\tname: "'"$rZone"'"' -e '$ a\\tzonefile: "'"$rZoneFile"'"\n' /etc/nsd/nsd.conf
done

for ((i=0;$i<$zoneNum;i++)); do
	cp ./nsd/templateZone /etc/nsd/${zoneFiles[$i]}
	sed -i -r -e 's/^(tmpDomain)(.*)(tmpHostname)(.*)/'"${zones[$i]}"'.\2'"$hostname"'.\4/' -e '$ a\'"$domain"'.\tIN\tNS\t'"$hostname"'.' /etc/nsd/${zoneFiles[$i]}
	if [[ $mail -eq "true" ]]; then
		sed -i -r -e '$ a\mail.'"$domain"'.\tIN\tMX\t10 mail.'"${zones[$i]}"'.\n' /etc/nsd/${zoneFiles[$i]}
	fi
	for ((k=0;$k<$aRecordNum;k++)); do
		sed -i -r -e '$ a\'"${aRecords[$k]}.${zones[$i]}"'.\tIN\tA\t'"${aRecordIPs[$k]}" /etc/nsd/${zoneFiles[$i]}
	done
done

for ((i=0;$i<$rZoneNum;i++)); do
	cp ./nsd/templateZone /etc/nsd/${rZoneFiles[$i]}
	sed -i -r -e 's/^(tmpDomain)(.*)(tmpHostname)(.*)/'"${rZones[$i]}"'.\2'"$hostname"'.\4/' -e '$ a\'"${rZones[$i]}"'.\tIN\tNS\t'"$hostname"'.' /etc/nsd/${rZoneFiles[$i]}
	for ((k=0;$k<$aRecordNum;k++)); do
		ip=($(splitDot ${aRecordIPs[$k]}))
		reverseIP=${ip[3]}.${rZones[$i]}
		sed -i -r -e '$ a\'"$reverseIP"'.\tIN\tPTR\t'"${aRecords[$k]}.${zones[$i]}"'.' /etc/nsd/${rZoneFiles[$i]}
	done
done

systemctl start nsd
systemctl enable nsd