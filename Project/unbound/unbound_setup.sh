#!/bin/bash - 

. ./split.sh

dns=$1
netIDs=($(splitComma $2))
netIPs=($(splitComma $3))
wifi=$4
wifiIDs=($(splitComma $5))
wifiIPs=($(splitComma $6))
zones=($(splitComma $7))
rZones=($(splitComma $8))
delete=(none)
totalIDs=(${netIDs[@]})
totalIPs=(${netIPs[@]})
totalZones=(${zones[@]} ${rZones[@]})

if [[ $wifi == "true" ]]; then
	totalIDs=(${totalIDs[@]} ${wifiIDs[@]})
	totalIPs=(${totalIPs[@]} ${wifiIPs[@]})
fi

totalIDs=( "${totalIDs[@]/$delete}" )

yum install -y unbound

wget -S -N https://www.internic.net/domain/named.cache -O /etc/unbound/root.hints

cp ./unbound/unbound.conf /etc/unbound/

for id in ${totalIDs[@]}; do
	sed -r -i '/^[[:space:]]+access-control:.*127.0.0.0/ a\\taccess-control: '"$id"' allow' /etc/unbound/unbound.conf
done

for ip in ${totalIPs[@]:1}; do
	ipArray=($(splitSlash $ip))
	sed -r -i '/^[[:space:]]+interface:[[:space:]]127.0.0.1/a\\tinterface: '"${ipArray[0]}" /etc/unbound/unbound.conf
done

#stub-zone:
#	name: "s02.as.learn"
#	stub-addr: 10.16.255.2
#stub-zone:
#	name: "2.16.10.in-addr.arpa"
#	stub-addr: 10.16.255.2

for zone in ${totalZones[@]}; do
	ipArray=($(splitSlash ${totalIPs[0]}))
	sed -r -i '$ a\stub-zone:\n\tname: '"$zone"'\n\tstub-addr: '"${ipArray[0]}" /etc/unbound/unbound.conf
done

systemctl start unbound
systemctl enable unbound

sed -r -i 's/^(DNS1=).*/\1'"$dns"'/' /etc/sysconfig/network-scripts/ifcfg-eth0

systemctl restart network