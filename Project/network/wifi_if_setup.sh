#!/bin/bash - 

yum install -y hostapd

. ./split.sh

interfaces=($(splitComma $1))
ips=($(splitComma $2))
ids=($(splitComma $3))
channels=($(splitComma $4))
names=($(splitComma $5))
encryptions=($(splitComma $6))
passes=($(splitComma $7))
numInts=${#interfaces[@]}
wpaExpress=""
wpaKeyExpress=""
rsnExpress=""
wpaPassExpress=""

cp ./network/hostapd.service /etc/systemd/system/multi-user.target.wants/

for ((i=0; i<$numInts; i++)); do
    interface=${interfaces[$i]}
    sepIP=($(splitSlash ${ips[$i]}))
    ip=${sepIP[0]}
    prefix=${sepIP[1]}
    channel=${channels[$i]}
    name=${names[$i]}
    encryption=${encryptions[$i]}
    pass=${passes[$i]}
	cp ./network/ifcfg-wifitemplate /etc/sysconfig/network-scripts/ifcfg-$interface
	sed -r -i -e 's/^(DEVICE=).*/\1'$interface'/' -e 's/^(IPADDR=).*/\1'$ip'/' -e 's/^(PREFIX=).*/\1'$prefix'/' -e 's/^(CHANNEL=).*/\1'$channel'/' -e 's/^(ESSID=).*/\1'$name'/' /etc/sysconfig/network-scripts/ifcfg-$interface
	path=""
	if [[ $i -eq 0 ]]; then
		path="/etc/hostapd/hostapd.conf"
	else
		path="/etc/hostapd/hostapd'"$i"'.conf"
	fi
	cp ./network/hostapd.conf "$path"
	if [[ $encryption == true ]]; then
		wpaExpress='s/^(wpa=).*/\12/'
		wpaKeyExpress='s/^(wpa_key_mgmt=).*/\1WPA-PSK/'
		rsnExpress='s/^(rsn_pairwise=).*/\1CCMP/'
		wpaPassExpress='s/^(wpa_passphrase=).*/\1'$pass'/'
	else
		wpaExpress='s/^(wpa=).*/#\1/'
		wpaKeyExpress='s/^(wpa_key_mgmt=).*/#\1/'
		rsnExpress='s/^(rsn_pairwise=).*/#\1/'
		wpaPassExpress='s/^(wpa_passphrase=).*/#\1/'
	fi
	sed -r -i -e 's/^(interface=).*/\1'$interface'/' -e 's/^(ssid=).*/\1'$name'/' -e 's/^(channel=).*/\1'$channel'/' -e "$wpaExpress" -e "$wpaKeyExpress" -e "$rsnExpress" -e "$wpaPassExpress" "$path"
	if [[ $i -ne 0 ]]; then
		path=$(echo $path | sed 's_/_\\/_g')
		sed -r -i 's/^(ExecStart=.*)(-P.*)/\1'$path' \2/' /etc/systemd/system/multi-user.target.wants/hostapd.service
	fi
done

systemctl daemon-reload
systemctl restart network

systemctl start hostapd
systemctl enable hostapd