#!/bin/bash - 

. ./split.sh

interfaces=($(splitComma $1))
ips=($(splitComma $2))
networkid=($(splitComma $3))
dnsVal=($(splitComma $4))
gateways=($(splitComma $5))
bootproto=($(splitComma $6))
numInts=${#interfaces[@]}
dnsExpress=""
gatewayExpress=""

systemctl stop NetworkManager firewalld
systemctl disable NetworkManager firewalld

for ((i=0; i<$numInts; i++)); do
    interface=${interfaces[$i]}
    sepIP=($(splitSlash ${ips[$i]}))
    ip=${sepIP[0]}
    prefix=${sepIP[1]}
    dns=${dnsVal[$i]}
    gateway=${gateways[$i]}
    cp ./network/ifcfg-template /etc/sysconfig/network-scripts/ifcfg-$interface
    if [[ "$dns" == "none" ]]; then
            dnsExpress='s/^(DNS1=).*/#\1/'
    else
            dnsExpress='s/^(DNS1=).*/\1'$dns'/'
    fi
    if [[ "$gateway" == "none" ]]; then
            gatewayExpress='s/^(GATEWAY=).*/#\1/'
    else
            gatewayExpress='s/^(GATEWAY=).*/\1'$gateway'/'
    fi
    sed -i -r -e 's/^(NAME=)=.*/\1'$interface'/' -e 's/^(DEVICE=).*/\1'$interface'/' -e 's/^(IPADDR=).*/\1'$ip'/' -e 's/^(PREFIX=).*/\1'$prefix'/' -e 's/^(BOOTPROTO=).*/\1'$bootproto'/' -e "$dnsExpress" -e "$gatewayExpress" /etc/sysconfig/network-scripts/ifcfg-$interface
done

systemctl restart network
