#!/bin/bash 

escapeSlashes() {
	echo ${1//\//\\/}
}

loginNets=$(escapeSlashes $1)
mailDir=$(escapeSlashes $2)
port=$3
key=$(escapeSlashes $4)
cert=$(escapeSlashes $5)


yum install -y dovecot

cp ./dovecot/dovecot.conf /etc/dovecot/dovecot.conf
sed -r -i -e 's/^(#|)[[:space:]]*(protocols[[:space:]]*=).*/\2 imap imaps pop3 pop3s/' -e 's/^(#|)[[:space:]]*(login_trusted_networks[[:space:]]*=).*/\2 '"$loginNets"'/' /etc/dovecot/dovecot.conf

cp ./dovecot/10-mail.conf /etc/dovecot/conf.d/10-mail.conf
sed -r -i 's/^(#|)(mail_location[[:space:]]*=).*/\2 '"$mailDir"'/' /etc/dovecot/conf.d/10-mail.conf

cp ./dovecot/10-auth.conf /etc/dovecot/conf.d/10-auth.conf
sed -r -i 's/^(#|)(disable_plaintext_auth[[:space:]]*=).*/\2 no/' /etc/dovecot/conf.d/10-auth.conf

groupRange='/unix_listener auth-userdb \{/,/^([[:space:]]*\})/'

cp ./dovecot/10-master.conf /etc/dovecot/conf.d/10-master.conf
sed -r -i -e "$groupRange"'{s/^([[:space:]]*)(#|)(mode =).*/\1\3 0660/}' -e "$groupRange"'{s/^([[:space:]]*)(#|)(user =).*/\1\3 postfix/}' -e "$groupRange"'{s/^([[:space:]]*)(#|)(group =).*/\1\3 postfix/}' /etc/dovecot/conf.d/10-master.conf

groupRange='/service auth \{/,/^(\})/!b;'
sed -r -i -e "$groupRange"'/(^\})/i\  inet_listener \{\n    port = '"$port"'\n  }' -e "$groupRange"'/(^\})/a\auth_mechanisms = plain login' /etc/dovecot/conf.d/10-master.conf

sed -r -i -e '$ a\protocols = imap imaps pop3 pop3s' -e '$ a\ssl_cert_file = '"$cert" -e '$ a\ssl_key_file = '"$key" -e '$ a\ssl_cipher_list = ALL:!LOW:!SSLv2' /etc/dovecot/dovecot.conf

systemctl enable dovecot
systemctl start dovecot
systemctl restart postfix