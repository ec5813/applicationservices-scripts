#!/bin/bash - 

. ./split.sh

findNetmask() {
	echo $(ifconfig | grep inet | sed -r -n 's/.*'"$1"'[[:space:]]+netmask[[:space:]]+([0-9]+.[0-9]+.[0-9]+.[0-9]+).*/\1/p')
}

interfaces=$(splitComma $1)
domain=$2
netIPs=($(splitComma $3))
netIDs=($(splitComma $4))
wifi=$5
wifiIPs=($(splitComma $6))
wifiIDs=($(splitComma $7))
staticNets=($(splitComma $8))
staticIPs=($(splitComma $9))
staticNames=($(splitComma ${10}))
staticMACs=($(splitComma ${11}))
ipRanges=($(splitComma "${@:12}"))
totalIPs=(${netIPs[@]})
totalIDs=(${netIDs[@]})
staticNum=${#staticIPs[@]}
delete=(none)

if [[ $wifi == "true" ]]; then
	totalIDs=(${totalIDs[@]} ${wifiIDs[@]})
	totalIPs=(${totalIPs[@]} ${wifiIPs[@]})
fi

totalIDs=( "${totalIDs[@]/$delete}" )

yum install -y dhcp

cp /usr/lib/systemd/system/dhcpd.service /etc/systemd/system/dhcpd.service

sed -i -r 's/^(Exec.*)/\1 '"$interfaces"'/' /etc/systemd/system/dhcpd.service

systemctl daemon-reload

cp ./dhcpd/dhcpd.conf /etc/dhcp/

ips=()
for ip in ${totalIPs[@]:1}; do
	splitIP=($(splitSlash $ip))
	ips+=(${splitIP[0]})
done

ips=${ips[*]}
ips=${ips// /,}

sed -i -r -e 's/^(option[[:space:]]+domain-name[[:space:]]+).*/\1"'"$domain"'";/' -e 's/^(option[[:space:]]+domain-name-servers[[:space:]]+).*/\1'"$ips"';/' /etc/dhcp/dhcpd.conf

i=0
for id in ${totalIDs[@]}; do
	idArray=($(splitSlash $id))
	ipArray=($(splitSlash ${totalIPs[($i+1)]}))
	netmask=($(findNetmask "${ipArray[0]}"))
	range=${ipRanges[@]:($i*2):2}
	range=${range[*]}
	sed -i -r -e '$ a\subnet '${idArray[0]}' netmask '"$netmask"' {' -e '$ a\\toption routers '"${ipArray[0]}"';' -e '$ a\\trange '"$range"';' -e '$ a\}' /etc/dhcp/dhcpd.conf
	let i++;
done

#subnet 10.16.2.0 netmask 255.255.255.128 {
#        option routers 10.16.2.126;
#        range 10.16.2.100 10.16.2.125;
#}

for ((i=0; $i<$staticNum; i++)); do
	sed -i -r -e '$ a\host '"${staticNames[$i]}"' {' -e '$ a\\thardware ethernet '"${staticMACs[$i]}"';' -e '$ a\\tfixed-address '"${staticIPs[$i]}"';' -e '$ a\\toption host-name "'"${staticNames[$i]}.$domain"'";' -e '$ a\}' /etc/dhcp/dhcpd.conf
done


#	host mail {
#		hardware ethernet 08:00:27:3A:1C:7C;
#		fixed-address 10.16.2.1;
#		option host-name "mail.s02.as.learn";
#	}

systemctl start dhcpd
systemctl enable dhcpd