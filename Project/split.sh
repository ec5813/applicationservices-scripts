#!/bin/bash

splitComma() {
		string=$*
        ret=($(split , "$string"))
        echo "${ret[@]}"
}

splitSlash() {
		string=$*
        ret=($(split / "$string"))
        echo "${ret[@]}"
}

splitDot() {
		string=$*
        ret=($(split . "$string"))
        echo "${ret[@]}"
}


split() {
	string=$2
	output=(${string//$1/ })
	echo "${output[@]}"
}
