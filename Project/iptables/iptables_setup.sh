#!/bin/bash

. ./split.sh

netInts=($(splitComma $1))
netIPs=($(splitComma $2))
netIDs=($(splitComma $3))
wifiEnabled=$4
wifiInts=($(splitComma $5))
wifiNum=${#wifiInts[@]}

###################################################################################################
#Preliminary Setup
###################################################################################################
#Flush Tables
iptables -F -t filter
iptables -F -t nat

#Delete all user Defined Chains
iptables -X

###################################################################################################
#Filter Input Rules (i.e. for datagrams comming into the router itself)
###################################################################################################

#Policy (i.e. the default)
iptables -t filter -P INPUT DROP

iptables -t filter -A INPUT -i lo -j ACCEPT

iptables -t filter -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

mainInt=${netInts[0]}
mainIP=($(splitSlash ${netIPs[0]}))
mailIP=($(splitSlash ${netIDs[1]}))

mailIP=$(echo "10.16.2.0" | sed -r 's/^([0-9]+.[0-9]+.[0-9]+.).*/\11/')

ints=(${netInts[@]:1})
if [[ $wifiEnabled == "true" ]]; then
	ints=(${ints[@]} ${wifiInts[@]})
fi

#Allow ssh to router
iptables -t filter -A INPUT -i $mainInt -d ${mainIP[0]} -p tcp --dport 22 -m state --state NEW -j ACCEPT
#Allow ssh to mail server
iptables -t filter -A INPUT -i $mainInt -d $mailIP -p tcp --dport 22 -m state --state NEW -j ACCEPT
#Allow ospf
iptables -t filter -A INPUT -i $mainInt -d 224.0.0.5 -p 89 -j ACCEPT

iptables -t filter -A INPUT -i $mainInt -d 224.0.0.6 -p 89 -j ACCEPT
#DNS to router
iptables -t filter -A INPUT -i $mainInt -p tcp --dport 53 -m state --state NEW -j ACCEPT
 
iptables -t filter -A INPUT -i $mainInt -p udp --dport 53 -j ACCEPT
#DNS to router from wifi
#if [[ $wifiEnabled == "true" ]]; then
for int in ${ints[@]}; do
	iptables -t filter -A INPUT -i $int -p tcp --dport 53 -m state --state NEW -j ACCEPT

	iptables -t filter -A INPUT -i $int -p udp --dport 53 -j ACCEPT

	iptables -t filter -A INPUT -i $int -p tcp --dport 67 -m state --state NEW -j ACCEPT

	iptables -t filter -A INPUT -i $int -p udp --dport 67 -j ACCEPT
done
#fi

#PING from all
iptables -t filter -A INPUT -p icmp -j ACCEPT 

###################################################################################################
#Filter Output Rules (i.e. for datagrams leaving the router itself)
###################################################################################################

#Policy (i.e. the default)
iptables -t filter -P OUTPUT ACCEPT

###################################################################################################
#Filter Forward Rules (i.e. for datagrams being forwarded by the router)
###################################################################################################

#Policy (i.e. the default)
iptables -t filter -P FORWARD ACCEPT

iptables-save > /etc/sysconfig/iptables
