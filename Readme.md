Scripts to setup the VMs are located in /Project/

#Hierarchy:
* main.sh - Main invocation script
	* main.conf - Configuration files for the entire script
* base_configuration.sh - Configure base system (Hostname, updates, etc)
* selinux_setup.sh - Configures SELINUX to permissive
* network_setup.sh - Invokes network and wireless interface setup
	* net_if_setup.sh - Configures the network interfaces
	* wifi_if_setup.sh - Configures the wifi interfaces and hostapd
* iptables_setup.sh - Configures iptable rules
* nsd_setup.sh - Configures authoritative name service
* unbound_setup.sh - Configures recursive name service
* dhcpd_setup.sh - Configures dhcpd service
* ospfd_setup.sh - Configures ospfd routing service
* postfix_setup.sh - Configures mail transfer agent
* dovecot_setup.sh - Configures mail delivery agent

#Base Configuration Requirements:
* Centos 7 VM
* Interfaces all up
* Connection to the internet
* All interfaces should be Paravirtualized
##Router settings:
	* Has minimum two interfaces
	* Interface 1 is bridged to the VLAN
	* Interface 2 and above is set to an internal network
	* Wifi is optional
##Mail server settings:
	* Has one interface set to one of the routers internal networks

#Running the script:
* Run the main script, main.sh, as a sudo user
* Use one of these options to setup a router or a mail server
	* -r
		* Sets up a router
	* -m
		* Sets up a mail server
##Example:
```
sudo ./main.sh -r
```

#Configuring the script
* Edit main.conf to suite your setup
* For network and wifi settings, if there is nothing use enter.
* To set multiple values for an option, use a comma to seperate it without any spaces
* dhcpIPRange has to have a space between the range
	* ie 10.10.10.10 10.10.10.11 or 10.10.10.10 10.10.10.11,10.10.10.20 10.10.10.21
* The first ip listed in networkIP will be used as the outfacing interface
* Hostapd is installed if wifi is enabled

